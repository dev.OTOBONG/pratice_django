from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class GoalStatus(models.Model):
    """Stores all possible status of a goal."""
    status_name = models.CharField(max_length=300)

    class Meta:
        verbose_name_plural = "goal status"

    def __str__(self):
        """ Returns a string representation of the model."""
        return self.status_name.title()


class ScrumyGoals(models.Model):
    """ keeps record of each goal."""
    goal_name = models.CharField(max_length=300)
    goal_id = models.IntegerField()
    created_by = models.CharField(max_length=300)
    moved_by = models.CharField(max_length=300)
    owner = models.CharField(max_length=300)
    goal_status = models.ForeignKey(GoalStatus, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="+")

    class Meta:
        verbose_name_plural = "scrumy goals"

    def __str__(self):
        """Returns a string representation of the modeel."""
        return self.goal_name.title()

class ScrumyHistory(models.Model):
    """Stores information about actions carried out on each goal."""
    moved_by = models.CharField(max_length=300)
    created_by = models.CharField(max_length=300)
    moved_from = models.CharField(max_length=300)
    moved_to = models.CharField(max_length=300)
    time_of_action = models.DateTimeField(default=timezone.now)
    goal = models.ForeignKey(ScrumyGoals, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "scrumy history"

    def __str__(self):
        """Returns a strng representation of the model."""
        return self.created_by.title()


