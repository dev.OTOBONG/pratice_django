from django.apps import AppConfig


class SamsonscrumyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'samsonscrumy'
