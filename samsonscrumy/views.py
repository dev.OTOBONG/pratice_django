from django.shortcuts import render
from django.http import HttpResponse
from .models import ScrumyGoals


def get_grading_parameters(request):
    """ Home page for samscrumy app. """
    # Filter object with goal_name field from ScrumyGoals Table
    filters = ScrumyGoals.objects.filter(goal_name="Learn Django")
    return HttpResponse(filters)
