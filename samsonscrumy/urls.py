from django.urls import path
from . import views

app_name = 'samsonscrumy'
urlpatterns = [
    # Home Page.
    path('', views.get_grading_parameters, name='views.get_grading_parameters'),

]